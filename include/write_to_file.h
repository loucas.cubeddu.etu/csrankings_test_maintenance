#pragma once

#include <stdio.h>
#include "program_options.h"

extern void write_to_file(FILE *write_file, FILE *read_file, struct ProgramOptions options);