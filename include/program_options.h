#pragma once

#include <stdbool.h>

enum FormatTypeOption
{
    OPTION_YAML = 0,
    OPTION_JSON = 1
};

enum FilterOptions
{
    OPTION_NAME = 1 << 0,
    OPTION_AFFILIATION = 1 << 1,
    OPTION_HOMEPAGE = 1 << 2,
    OPTION_SCHOLARID = 1 << 3
};

struct ProgramOptions
{
    enum FormatTypeOption output_format;
    const char *output_file;
    enum FilterOptions filters;
    const char *affiliation;
};

extern struct ProgramOptions program_options_create();

extern bool program_options_init_argv(struct ProgramOptions *options, int argc, char *const argv[]);