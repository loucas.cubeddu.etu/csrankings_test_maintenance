# csrankings_test_maintenance

Projet TP de test et maintenance (IS4)

[Voir le sujet](https://gitlab.univ-lille.fr/jeremie.dequidt/is4_projet)


Le projet n'est pas terminé, la CI n'a jamais fonctionné ni été proche de fonctionner. Je m'y suis pris trop tard ayant priorisé d'autres projets.

Pas de couverture de tests, les tests sont seulement pour tout le module option et pas unitaires.

Build:

```
git clone https://gitlab.univ-lille.fr/loucas.cubeddu.etu/csrankings_test_maintenance.git
	OR
git clone git@gitlab-ssh.univ-lille.fr:loucas.cubeddu.etu/csrankings_test_maintenance.git
git submodule update --init --recursive

cmake .
cb build/
make # sur linux
# MSVC sur windows, gcc sur toutes plateformes.
```



Suivre la convention de nomage suivante : https://stackoverflow.com/a/1722518/7099546

Build system: CMake pour la portabilité

## Réalisation

### Options de ligne de commande

J'utilisais `optargs` au début du projet, mais ce n'était pas cross-platform (utilitaire linux).

J'ai donc changé pour utiliser [`libcargs`](https://github.com/likle/cargs), qui est similaire et cross-platform.

Je définis une structure représentant les options de mon programme :

```c
enum FormatTypeOption
{
	OPTION_YAML = 0,
    OPTION_JSON = 1
};

enum FilterOptions
{
	OPTION_NAME = 1 << 0,
    OPTION_AFFILIATION = 1 << 1,
    OPTION_HOMEPAGE = 1 << 2,
    OPTION_SCHOLARID = 1 << 3
};

struct ProgramOptions
{
    enum FormatTypeOption output_format;
    const char *output_file;
    enum FilterOptions filters;
    const char *affiliation;
};
```

Dans le main, voilà comment j'initialise les options

```c
struct ProgramOptions options = 
	program_options_create();
bool success = program_options_init_argv(&options, argc, argv);
```

## Lecture des données

J'ai d'abord créé des string dynamiques avant de me rendre compte que ça ne rendais pas la tâche plus facile. Donc j'ai supprimé cette fonctionnalité:


```c
struct DynamicString
{
	size_t buffer_length;
	size_t string_length;
	char *buffer;
};

struct Researcher
{
	struct DynamicString name;
	struct DynamicString affiliation;
	struct DynamicString homepage;
	struct DynamicString scholarid;
}
```


```c
	struct DynamicString line = dynamic_string_create(128);
	dynamic_string_get_line(&line, fp);
	printf("%s\n", dynamic_string_get_string(&line));
		...
	dynamic_string_destroy(&line);
```

```c
	struct Researcher researcher = researcher_create_from_line(line);
	printf("%s\n", researcher_get_name_string(&researcher));
		...
	reasearcher_destroy(&researcher);
```

## Écriture des données

J'ai voulu réécrire l'application proprement, mais je n'ai pas eu le temps, et je préfère donc consacrer le temps qu'il me reste à la CI et gitlab.

L'application limite donc la taille d'une ligne à 2048 caractères.

J'utilise fscanf pour parser le csv.
