/**
 * @brief      This file implements unit tests for CSV parsing, and YAML and JSON generation.
 *
 * @author     Loucas
 * @date       2023
 */

#include <check.h>
#include <stdlib.h>
#include "program_options.h"

START_TEST(test_options) {
    int argc = 9;
    const char *argv[] = {"csrankings_test_maintenance", "-s", "abcd", "-o", "abdc", "-f", "nhhhsshhs", "-t", "json"};

    struct ProgramOptions options = program_options_create();
    program_options_init_argv(&options, argc, argv[]);

    ck_assert_int_eq(options.filters, (OPTION_NAME | OPTION_HOMEPAGE | OPTION_SCHOLARID));
    ck_assert_int_eq(options.output_format, OPTION_JSON);
    ck_assert_str_eq(options.output_file, "abdc");
    ck_assert_str_eq(options.affiliation, "abcd");
}
END_TEST

START_TEST(test_options_default) {
    int argc = 1;
    const char *argv[] = {"csrankings_test_maintenance"};
    
    
    struct ProgramOptions options = program_options_create();
    program_options_init_argv(&options, argc, argv[]);

    ck_assert_int_eq(options.filters, (OPTION_NAME | OPTION_AFFILIATION | OPTION_HOMEPAGE | OPTION_SCHOLARID));
    ck_assert_int_eq(options.output_format, OPTION_YAML);
    ck_assert_ptr_null(options.output_file);
    ck_assert_ptr_null(options.affiliation);
}
END_TEST

Suite *sort_suite (void)
{
    Suite *s       = suite_create ("PassTests");
    TCase *tc_core = tcase_create ("Core");
    tcase_add_test (tc_core, test_options);
    tcase_add_test (tc_core, test_options_default);
    suite_add_tcase (s, tc_core);

    return s;
}

int main (void)
{
    int      no_failed = 0;
    Suite   *s         = sort_suite ();
    SRunner *runner    = srunner_create (s);
    srunner_run_all (runner, CK_NORMAL);
    no_failed = srunner_ntests_failed (runner);
    srunner_free (runner);
    return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
