/**
 * @brief This file is a command line app which executes a simple query
 * on the csrankings.csv file from https://csrankings.org/ .
 * It outputs its results in YAML (default) or JSON.
 * It can filter columns and search scholars by affiliation to an institution.
 *
 * USAGE:
 * ./projet [OPTIONS] csv_file
 *
 * OPTIONS:
 * : -t [yaml, json]specifies the output format. If this option is not used, the
 * default value is yaml. -o [filename]: allows you to save the result in the
 * specified file. If this option is not used, the program displays the result
 *on the standard output. -f [nahs]: selects the information to display for each
 * researcher (n for the name, a for the affiliation, h for the homepage, s for
 * the Google Scholar profile). Multiple filters can be used, for example, -f n
 * -f a, or -f na displays the names and affiliations of the researchers. -s
 * "Affiliation": searches only for researchers affiliated with an institution.
 * EXAMPLES:
 * ./projet -t yaml -f ns -s "University of Hildesheim"
 * ./projet -o output.json -f nah -s "University of California, Los Angeles"
 * ./projet -f nahs
 *
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "program_options.h"
#include "write_to_file.h"

int main(int argc, char *const argv[]) 
{
    struct ProgramOptions options = program_options_create();
    bool options_success
        = program_options_init_argv(&options, argc, argv);

    if(!options_success)
        exit(EXIT_FAILURE);

    FILE* read_file = fopen("../ressources/csrankings.csv", "r");
    if(!read_file)
    {
        fprintf(stderr, "Failed to open file.\n");
        exit(EXIT_FAILURE);
    }

    FILE* write_file = stdout;
    if(options.output_file != NULL)
    {
        write_file = fopen(options.output_file, "w");
        if(!write_file)
        {
            fprintf(stderr, "Failed to open file.\n");
            exit(EXIT_FAILURE);
        }
    }

    write_to_file(write_file, read_file, options);
}