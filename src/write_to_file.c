#include "write_to_file.h"

#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#include "program_options.h"

#define MAX_LINE_LENGTH 2048

static void escaped_quotes_write(const char* str, FILE* f)
{
    int n = strlen(str);

    for(int i = 0; i < n; i++)
    {
        char ch = str[i];

        if (ch =='"')
            fputs("\\\"", f);
        else
            fputc(ch, f);
    }
}

extern void write_to_file(FILE *write_file, FILE *read_file, struct ProgramOptions options)
{
    if(options.output_format == OPTION_YAML)
        fprintf(write_file, "---\n");
    
    if(options.affiliation != NULL)
    {
        if(options.output_format == OPTION_YAML)
        {
            fprintf(write_file, "affiliation: \"%s\"\n", options.affiliation);
            fprintf(write_file, "members:\n");
        }
        else if (options.output_format == OPTION_JSON)
        {
            fprintf(write_file, "{\n");
            fprintf(write_file, "\t\"affiliation\": \"%s\",\n", options.affiliation);
            fprintf(write_file, "\t\"members\": ");
        }
    }

    if(options.output_format == OPTION_JSON)
        fprintf(write_file, "[\n");

    char line[MAX_LINE_LENGTH];
    fgets(line, MAX_LINE_LENGTH, read_file);
    while (fgets(line, MAX_LINE_LENGTH, read_file) != NULL) 
    {
        char name[MAX_LINE_LENGTH];
        char affiliation[MAX_LINE_LENGTH];
        char homepage[MAX_LINE_LENGTH];
        char scholarid[MAX_LINE_LENGTH];
        sscanf(line, "%[^,],%[^,],%[^,],%s",
         name, affiliation, homepage, scholarid);
        
        if(options.affiliation == NULL || strcmp(affiliation, options.affiliation) == 0)
        {
            if(options.output_format == OPTION_YAML)
            {
                fprintf(write_file, "\t-\n");
                if(options.filters & OPTION_NAME)
                {
                    fprintf(write_file, "\t\tname: \"");
                    escaped_quotes_write(name, write_file);
                    fprintf(write_file, "\"\n");
                }
                if(options.filters & OPTION_AFFILIATION)
                {
                    fprintf(write_file, "\t\taffiliation: \"%s\"\n", affiliation);
                }
                if(options.filters & OPTION_HOMEPAGE)
                {
                    fprintf(write_file, "\t\thomepage: \"%s\"\n", homepage);
                }
                if(options.filters & OPTION_SCHOLARID)
                {
                    fprintf(write_file, "\t\tscholar: %s\n", scholarid);
                }
            }
            else
            {
                if(options.affiliation != NULL)
                    fprintf(write_file, "\t");
                fprintf(write_file, "\t{\n");

                if(options.filters & OPTION_NAME)
                {
                    if(options.affiliation != NULL)
                        fprintf(write_file, "\t");
                    fprintf(write_file, "\t\t\"name\": \"");
                    escaped_quotes_write(name, write_file);
                    fprintf(write_file, "\",\n");
                }
                if(options.filters & OPTION_AFFILIATION)
                {
                    if(options.affiliation != NULL)
                        fprintf(write_file, "\t");
                    fprintf(write_file, "\t\t\"affiliation\": \"%s\",\n", affiliation);
                }
                if(options.filters & OPTION_HOMEPAGE)
                {
                    if(options.affiliation != NULL)
                        fprintf(write_file, "\t");
                    fprintf(write_file, "\t\t\"homepage\": \"%s\",\n", homepage);
                }
                if(options.filters & OPTION_SCHOLARID)
                {
                    if(options.affiliation != NULL)
                        fprintf(write_file, "\t");
                    fprintf(write_file, "\t\t\"scholar\": \"%s\"\n", scholarid);
                }

                if(options.affiliation != NULL)
                    fprintf(write_file, "\t");
                fprintf(write_file, "\t},\n");
            }
        }
    }

    if(options.output_format == OPTION_JSON)
    {
        if(options.affiliation != NULL)
            fprintf(write_file, "\t");
        fprintf(write_file, "]\n");
        if(options.affiliation != NULL)
            fprintf(write_file, "}\n");
    }
}
