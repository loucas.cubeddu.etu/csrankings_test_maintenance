#include "program_options.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <cargs.h>

/**
 * On utilse cargs à la place de optargs afin de garder le
 * programme cross-platform pour Windows, Linux et MacOS
*/

static struct cag_option cag_options[] = 
{
    {
        .identifier = 't',
        .access_letters = "t",
        .value_name = "OUTPUT_FORMAT_VALUE",
        .description = "Output format type [yaml|json]"
    },
    {
        .identifier = 'o',
        .access_letters = "o",
        .value_name = "OUTPUT_FILE_VALUE",
        .description = "Output file"
    },
    {
        .identifier = 'f',
        .access_letters = "f",
        .value_name = "FILTERS_VALUE",
        .description = "Selects the information to display for each researcher (n for the name, "
            "a for the affiliation, h for the homepage, s for the Google Scholar profile)"
    },
    {
        .identifier = 's',
        .access_letters = "s",
        .value_name = "AFFILIATION_VALUE",
        .description = "Searches only for researchers affiliated with a specified institution"
    },
    {
        .identifier = 'h',
        .access_letters = "h",
        .access_name = "help",
        .description = "Shows the command's help"
        }
};

struct ProgramOptions program_options_create()
{
    struct ProgramOptions options;

    options.output_format = 0;
    options.output_file = NULL;
    options.filters = 0;
    options.affiliation = NULL;

    return options;
}

static bool 
    program_options_update_output_format(struct ProgramOptions *options, const char *value)
{
    if(value == NULL)
    {
        fprintf(stderr, "You must enter an output format.\n");
        return false;
    }

    bool is_yaml = strcmp(value, "yaml");
    bool is_json = strcmp(value, "yaml");

    if(!is_yaml && !is_json)
    {
       fprintf(stderr, "Format must be either yaml or json.\n");
        return false;
    }

    if(is_yaml)
        options->output_format = OPTION_YAML;

    if(is_json)
        options->output_format = OPTION_JSON;

    return true;
}

static bool 
    program_options_update_output_file(struct ProgramOptions *options, const char *value)
{
    if(value == NULL)
    {
        fprintf(stderr, "You must enter an output file.\n");
        return false;
    }

    options->output_file = value;

    return true;
}

static bool 
    program_options_update_filters(struct ProgramOptions *options, const char *value)
{
    if(value == NULL)
    {
        fprintf(stderr, "You must enter a filter.\n");
        return false;
    }

    for(int i = 0; i < strlen(value); i++)
    {
        char c = value[i];
        switch ((c))
        {
        case 'n':
            options->filters |= OPTION_NAME;
            break;

        case 'a':
            options->filters |= OPTION_AFFILIATION;
            break;

        case 'h':
            options->filters |= OPTION_HOMEPAGE;
            break;

        case 's':
            options->filters |= OPTION_SCHOLARID;
            break;
            
        default:
            fprintf(stderr, "Filter must be a subset of {n, a, h, s}\n");
            return false;
        }
    }

    return true;
}

static bool 
    program_options_update_affiliation(struct ProgramOptions *options, const char *value)
{
    if(value == NULL)
    {
        fprintf(stderr, "You must enter an affiliation.\n");
        return false;
    }

    options->affiliation = value;

    return true;
}

bool program_options_init_argv(struct ProgramOptions *options, int argc, char *const argv[])
{
    cag_option_context context;
    cag_option_prepare(&context, cag_options, CAG_ARRAY_SIZE(cag_options), argc, (char**)argv);

    const char *filename = argv[0];
    
    bool success = true;
    while(cag_option_fetch(&context))
    {
        char identifier = cag_option_get(&context);

        const char *value;
        switch(identifier)
        {
            case 't':
                value = cag_option_get_value(&context);
                success = program_options_update_output_format(options, value);
                break;

            case 'o':
                value = cag_option_get_value(&context);
                success = program_options_update_output_file(options, value);
                break;

            case 'f':
                value = cag_option_get_value(&context);
                success = program_options_update_filters(options, value);
                break;

            case 's':
                value = cag_option_get_value(&context);
                success = program_options_update_affiliation(options, value);
                break;

            case 'h':
                success = false;
                fprintf(stdout,
                    "Usage : %s [-t yaml|json] [-o output_file] "
                    "[-f nahs] [-s school]\n", filename);
                cag_option_print(cag_options, CAG_ARRAY_SIZE(cag_options), stdout);
                break;

            default:
                success = false;
                fprintf(stderr, "Option doesn't exist.\n");
                cag_option_print(cag_options, CAG_ARRAY_SIZE(cag_options), stdout);
                break;
        }

        if(!success)
            return success;
    }
    
    if(options->filters == 0)
        options->filters = OPTION_NAME | OPTION_AFFILIATION | OPTION_HOMEPAGE | OPTION_SCHOLARID;

    return success;
}